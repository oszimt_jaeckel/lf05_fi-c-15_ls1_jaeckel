import java.util.ArrayList;

/**
 * 
 * @author marvin.jaeckel
 * @version 1.0
 *
 */

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	
	// Konstruktoren
	public Raumschiff() {
		
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent,
			          int zustandSchildeInProzent, int zustandHuelleInProzent,
			          int zustandLebenserhaltungssystemeInProzent,
			          int anzahlDroiden, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl = anzahlDroiden;
		this.schiffsname = schiffsname;
	}
	
	
	// getter & setter
	/**
	 * Gibt die anzahl der Photonentorpedos, die an Bord sind zur�ck
	 * @return int die PhotonentorpedoAnzahl
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * Setzt die PhotonentorpedoAnzahl zum gew�nschten Wert
	 * @param photonentorpedoAnzahl der zusetzende Wert f�r die photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * Gibt die EnergieversorgungInProzent des Raumschiffes zur�ck
	 * @return int Energieversorgung in Prozent
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * Setzt die EnergieversorgungInProzent zum gew�nschten Wert
	 * @param energieversorgungInProzent der zusetzende Wert f�r die Energieversorgung
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * Gibt SchildeInProzent des Raumschiffes zur�ck
	 * @return int Schilde in Prozent
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * Setzt SchildeInProzent zum gew�nschten Wert
	 * @param schildeInProzent der zusetzende Wert f�r die Schilde
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	/**
	 * Gibt die Huelle in Prozent des Raumschiffes zur�ck
	 * @return int Huelle in Prozent
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * Setzt HuelleInProzent zum gew�nschten Wert
	 * @param huelleInProzent der zusetzende Wert f�r die Huelle
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**
	 * Gibt die Lebenserhaltungssysteme des Raumschiffes zur�ck
	 * @return int lebenserhaltungssysteme in Prozent
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * Setzt die Lebenserhaltungssysteme zum gew�nschten Wert
	 * @param lebenserhaltungssystemeInProzent der zusetzende Wert f�r die Lebenserhaltungssysteme
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	/**
	 * Gibt die Androiden anzahl des Raumschiffes zur�ck
	 * @return int AndroidenAnzahl
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * Setzt die Androiden anzahl zum gew�nschten Wert
	 * @param androidenAnzahl der zusetzende Wert f�r die Androiden anzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	/**
	 * Gibt den Namen des Schiffes zur�ck
	 * @return schiffsname der Name des Schiffes
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * Setzt den Namen des Schiffes zum gew�nschten Wert
	 * @param schiffsname der zusetzende Wert f�r den Schiffsnamen
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	
	// methoden
	/**
	 * Gibt die Werte des Raumschiffes auf der Konsole aus
	 */
	public void zustandRaumschiff() {
		System.out.printf("Zustand f�r Schiff %s%n", schiffsname);
		System.out.println("---------------------------------------");
		System.out.printf("Photonentorpedos: %d%n", photonentorpedoAnzahl);
		System.out.printf("Energieversorgung in Prozent: %d%n", energieversorgungInProzent);
		System.out.printf("Schilde in Prozent: %d%n", schildeInProzent);
		System.out.printf("Huelle in Prozent: %d%n", huelleInProzent);
		System.out.printf("Lebenserhaltungssysteme in Prozent: %d%n", lebenserhaltungssystemeInProzent);
		System.out.printf("Androiden: %d%n%n%n", androidenAnzahl);
	}
	
	/**
	 * F�gt eine Ladung zum ladungsverzeichnis hinzu
	 * @param neueLadung die Ladung, die zum Ladungsverzeichnis hinzugef�gt werden soll
	 */
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Schie�t ein Photonentorpedo auf das angegebene Raumschiff
	 * @param r - das zu treffene Raumschiff
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(photonentorpedoAnzahl > 0) {
			photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	
	/**
	 * Schie�t mit der Phaserkanone auf das angegebene Raumschiff
	 * @param r - das zu treffene Raumschiff
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(energieversorgungInProzent >= 50) {
			energieversorgungInProzent = energieversorgungInProzent / 2;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	
	/**
	 * Berechnet den Schaden des getroffenen Raumschiffes und passt die Werte entsprechend an
	 * @param r - das zu treffene Raumschiff
	 */
	private void treffer(Raumschiff r) {
		System.out.printf("%s wurde getroffen!%n", r.getSchiffsname());
		
		r.setSchildeInProzent(r.getSchildeInProzent() / 2);
		
		if(r.getSchildeInProzent() <= 0) {
			r.setHuelleInProzent(r.getHuelleInProzent() / 2);
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() / 2);
			
			if(r.getHuelleInProzent() <= 0) {
				r.setLebenserhaltungssystemeInProzent(0);
				nachrichtAnAlle(r.getSchiffsname() + ": Lebenserhaltungssysteme sind komplett zerst�rt");
			}
		}
	}
	
	/**
	 * Speichert eine Nachricht in den broadcastKommunikator
	 * @param message - die zusendene Nachricht
	 */
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}

	/**
	 * Gibt den broadcastKommunikaor zur�ck
	 * @return broadcastKommunikator
	 */
	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}
	
	/**
	 * L�dt Photonentorpedos aus dem ladungsverzeichnis, falls Photonentorpedos
	 * geladen sind, sonst wird auf der Console "Keine Photonentorpedos gefunden!" ausgegeben
	 * @param anzahlTorpedos - anzahl der zuladenen Photonentorpedos
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		int eingesetzteTorpedos = 0;
		boolean hatPhotonentorpedosGeladen = false;
		
		
		
		for(Ladung ladung : ladungsverzeichnis) {
			if(ladung.getBezeichnung().equals("Photonentorpedo")) {
				hatPhotonentorpedosGeladen = true;
				
				if(ladung.getMenge() < anzahlTorpedos) {
					this.photonentorpedoAnzahl += ladung.getMenge();
					eingesetzteTorpedos = ladung.getMenge();
					ladungsverzeichnis.remove(ladung);
				} else {
					this.photonentorpedoAnzahl += anzahlTorpedos;
					eingesetzteTorpedos = anzahlTorpedos;
					ladung.setMenge(ladung.getMenge() - anzahlTorpedos);
				}
				
				break;
			}
		}
		
		if(hatPhotonentorpedosGeladen) {
			System.out.printf("%d Photonentorpedos eingesetzt%n", eingesetzteTorpedos);
		} else {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	
	/**
	 * Repariert die angegebenen Stellen
	 * @param schutzschilde - true wenn schutzschilde repariert werden sollen, sonst false
	 * @param energieversorgung - true wenn energieversorung repariert werden sollen, sonst false
	 * @param schiffshuelle - true wenn schiffshuelle repariert werden sollen, sonst false
	 * @param anzahlDroiden - anzahl der Droiden, die zur Reparatur benutzt werden sollen
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,
                                      boolean schiffshuelle, int anzahlDroiden)	 {
		int zufallsZahl = (int) (Math.random() * 100);
		
		if(zufallsZahl > 0) {
			int anzahlZuReparierentenStellen = 0;
			int verfuegbareDroiden = 0;
			
			
			if(schutzschilde) anzahlZuReparierentenStellen++;
			if(energieversorgung) anzahlZuReparierentenStellen++;
			if(schiffshuelle) anzahlZuReparierentenStellen++;
			
			if(this.androidenAnzahl < anzahlDroiden) {
				verfuegbareDroiden = this.androidenAnzahl;
			} else {
				verfuegbareDroiden = anzahlDroiden;
			}
			
			int prozentReparatur = (zufallsZahl * verfuegbareDroiden) / anzahlZuReparierentenStellen;
			
			if(schutzschilde) this.schildeInProzent += prozentReparatur;
			if(energieversorgung) this.energieversorgungInProzent += prozentReparatur;
			if(schiffshuelle) this.huelleInProzent += prozentReparatur;
		}
	}
	
	/**
	 * Gibt das ladungsverzeichnis auf der Konsole aus
	 */
	public void ladungsverzeichnisAusgeben() {
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			if(i != 0) {
				System.out.print("; ");
			}
			
			System.out.print(ladungsverzeichnis.get(i));
		}
		System.out.println("");
	}
	
	/**
	 * L�scht Ladungen aus dem Ladungsverzeichnis, wenn die Menge kleiner oder gleich 0 ist
	 */
	public void ladungsverzeichnisAufraeumen() {
		for(Ladung ladung : ladungsverzeichnis) {
			if(ladung.getMenge() <= 0) {
				ladungsverzeichnis.remove(ladung);
			}
		}
	}
}
