
public class ArtikelTest {

	public static void main(String[] args) {
		Artikel a1 = new Artikel(12, "Himalayareis", 1.41, 1.78, 412, 100);
		Artikel a2 = new Artikel(581, "Frische Fische von Fischers Fritze", 7.21, 12.45, 3, 10);
		
		System.out.printf("Gewinnmarge %s: %.2f €%n", a1.getBezeichnung(), a1.berechneGewinnMarge());
		System.out.printf("Gewinnmarge %s: %.2f €%n", a2.getBezeichnung(), a2.berechneGewinnMarge());
	}
}
