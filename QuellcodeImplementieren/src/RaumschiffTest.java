import java.util.Random;

/**
 * 
 * @author marvin.jaeckel
 * @version 1.0 
 */
public class RaumschiffTest {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Random random = new Random();
		
		
		Raumschiff klingonen = new Raumschiff(0, 100, 50, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 50, 50, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff();
		
		
		vulkanier.setPhotonentorpedoAnzahl(3);
		vulkanier.setEnergieversorgungInProzent(random.nextInt(100) + 1);
		vulkanier.setSchildeInProzent(random.nextInt(100) + 1);
		vulkanier.setHuelleInProzent(random.nextInt(100) + 1);
		vulkanier.setLebenserhaltungssystemeInProzent(100);
		vulkanier.setSchiffsname("Ni'Var");
		vulkanier.setAndroidenAnzahl(5);
		
		
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		
		
		// Ausführung der Methoden
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());
		vulkanier.photonentorpedosLaden(10);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		
		
		// broadcastKommunikator ausgeben
		for(String nachricht : Raumschiff.eintraegeLogbuchZurueckgeben()) {
			System.out.println(nachricht);
		}
	}
}
