
public class Artikel {

	private int artikelNummer;
	private String bezeichnung;
	private double einkaufspreis;
	private double verkaufspreis;
	private int bestand;
	private int sollbestand;
	
	
	// Constructor
	public Artikel() {
		
	}
	
	public Artikel(int artikelNummer, String bezeichnung, double einkaufspreis, double verkaufspreis, int bestand, int sollbestand) {
		this.artikelNummer = artikelNummer;
		this.bezeichnung = bezeichnung;
		this.einkaufspreis = einkaufspreis;
		this.verkaufspreis = verkaufspreis;
		this.bestand = bestand;
		this.sollbestand = sollbestand;
	}
	
	
	// getter and setter
	public int getArtikelNummer() {
		return artikelNummer;
	}

	public void setArtikelNummer(int artikelNummer) {
		this.artikelNummer = artikelNummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getEinkaufspreis() {
		return einkaufspreis;
	}

	public void setEinkaufspreis(double einkaufspreis) {
		this.einkaufspreis = einkaufspreis;
	}

	public double getVerkaufspreis() {
		return verkaufspreis;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getSollbestand() {
		return sollbestand;
	}

	public void setSollbestand(int sollbestand) {
		this.sollbestand = sollbestand;
	}
	
	
	// methods
	public void artikelAutomatischBestellen() {
		if(bestand <= sollbestand * 0.8) {
			// TODO: Artikel bestellen
		}
	}
	
	public void aendereLagerbestand(int anzahl) {
		this.bestand -= anzahl;
	}
	
	public double berechneGewinnMarge() {
		return verkaufspreis - einkaufspreis;
	}
}