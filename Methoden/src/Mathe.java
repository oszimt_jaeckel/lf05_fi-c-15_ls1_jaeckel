import java.util.Scanner;

public class Mathe {
	static Scanner myScanner;
	
	public static void main(String[] args) {
		myScanner = new Scanner(System.in);
		
		double zahl1 = liesDouble("Bitte geben sie eine Zahl ein: ");
		double zahl2 = liesDouble("Bitte geben sie eine zweite Zahl ein: ");
		
		System.out.println(zahl1 + " zum quadtrad ist " + quadrat(zahl1));
		System.out.println(zahl2 + " zum quadtrad ist " + quadrat(zahl2));
		
		System.out.println("\n\nDie hypotenuse ist: " + hypotenuse(zahl1, zahl2));
	}
	
	public static double liesDouble(String text) {
		System.out.print("Bitte geben sie eine Zahl ein: ");
		return myScanner.nextDouble();
	}
	
	public static double quadrat(double x) {
		return x * x;
	}
	
	public static double hypotenuse(double kathete1, double kathete2) {
		return Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
	}
}
