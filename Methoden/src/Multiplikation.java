import java.util.Scanner;

public class Multiplikation {
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("1. Wert: ");
		double zahl1 = myScanner.nextDouble();
		
		System.out.print("2. Wert: ");
		double zahl2 = myScanner.nextDouble();
		
		System.out.println("Ergebnis: " + multiply(zahl1, zahl2));
		
	}
	
	public static double multiply(double zahl1, double zahl2) {
		return zahl1 * zahl2;
	}
}
