import java.util.Scanner;

public class Mittelwert {
static double x, y, m;

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
	  Scanner myScanner = new Scanner(System.in);
	  
	  System.out.print("Bitte geben sie eine Zahl an: ");
      x = myScanner.nextDouble();
      
      System.out.print("Bitte geben sie eine zweite Zahl an: ");
      y = myScanner.nextDouble();
      


      // (V) Verarbeitung
      // Mittelwert von x und y berechnen:
      // ================================
      m = berechneMittelwert(x, y);

      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      ausgabe();
      myScanner.close();
   }
   
   private static void ausgabe() {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
   private static double berechneMittelwert(double num1, double num2) {
      return (num1 + num2) / 2.0;
   }
}
