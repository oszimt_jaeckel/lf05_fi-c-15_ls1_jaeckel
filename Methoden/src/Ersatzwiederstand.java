import java.util.Scanner;

public class Ersatzwiederstand {
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Geben sie bitte eine Zahl ein: ");
		double r1 = myScanner.nextDouble();
		
		System.out.print("Geben sie bitte eine zweite Zahl ein: ");
		double r2 = myScanner.nextDouble();
		
		double reihenschaltungErgebnis = reihenschaltung(r1, r2);
		double parallelschaltungErgebnis = parallelschaltung(r1, r2);
		
		System.out.println("Das ergebniss der Reihenschaltung ist: " + reihenschaltungErgebnis);
		System.out.println("Das ergebniss der Parallelschaltung ist: " + parallelschaltungErgebnis);
	}
	
	public static double reihenschaltung(double r1, double r2) {
		return r1 + r2;
	}
	
	public static double parallelschaltung(double r1, double r2) {
		return (r1 * r2) / (r1 + r2);
	}
}
