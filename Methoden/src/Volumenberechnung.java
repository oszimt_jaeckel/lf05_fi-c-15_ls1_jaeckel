import java.util.Scanner;

public class Volumenberechnung {
	static Scanner myScanner;
	
	public static void main(String[] args) {
		myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben sie einen Körper (Würfel, Quader, Pyramide, Kugel) an: ");
		String koerper = myScanner.next();
		
		if(koerper.equals("Würfel")) {
			System.out.println("Das Volumen vom Würfel ist: " + wuerfelVolumen());
		} else if(koerper.equals("Quader")) {
			System.out.println("Das Volumen vom Quader ist: " + quaderVolumen());
		} else if(koerper.equals("Pyramide")) {
			System.out.println("Das Volumen vom Pyramide ist: " + pyramideVolumen());
		} else if(koerper.equals("Kugel")) {
			System.out.println("Das Volumen vom Kugel ist: " + kugelVolumen());
		} else {
			System.out.println("Das war leider keine Gültige eingabe :(");
		}
	}
	
	public static double liesDouble(String text) {
		System.out.print(text);
		return myScanner.nextDouble();
	}
	
	public static double wuerfelVolumen() {
		double seitenlaenge = liesDouble("Bitte geben sie die Seitenlänge an: ");
		return seitenlaenge * seitenlaenge * seitenlaenge;
	}
	
	public static double quaderVolumen() {
		double seitenlaenge1 = liesDouble("Bitte geben sie die erste Seitenlänge an: ");
		double seitenlaenge2 = liesDouble("Bitte geben sie die zweite Seitenlänge an: ");
		double seitenlaenge3 = liesDouble("Bitte geben sie die dritte Seitenlänge an: ");
		return seitenlaenge1 * seitenlaenge2 * seitenlaenge3;
	}
	
	public static double pyramideVolumen() {
		double seitenlaenge = liesDouble("Bitte geben sie die Seitenlänge an: ");
		double hoehe = liesDouble("Bitte geben sie die Höhe an: ");
		return seitenlaenge * seitenlaenge * (hoehe / 3);
	}
	
	public static double kugelVolumen() {
		double radius = liesDouble("Bitte geben sie den Radius an: ");
		System.out.println(Math.pow(radius, 3.0));
		System.out.println(Math.PI);
		return ((double) 4/3) * Math.PI * Math.pow(radius, 3);
	}
}
