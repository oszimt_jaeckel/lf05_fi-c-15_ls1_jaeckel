import java.util.Scanner;

public class Fahrsimulator {
	static Scanner myScanner;
	static double v;  // Geschwindigkeit
	static double dv; // User input geschwindigkeit
	static double neueGeschwindigkeit;
	static boolean weiterSpielen;
	
	public static void main(String[] args) {
		initComponents();
		
		ausgabeSpielregeln();
		
		while(weiterSpielen) {
			liesUserinput();
			
			v = beschleunige(v, dv);
			
			ausgabeNeueGeschwindigkeit(v);
		}
		
	}
	
	public static void initComponents() {
		weiterSpielen = true;
		v = 0.0;
		myScanner = new Scanner(System.in);
	}
	
	public static void ausgabeSpielregeln() {
		System.out.println("Hallo, ich binn ein Fahrsimulator.");
		System.out.println("Wenn sie schneller fahren wollen, geben sie bitte eine positive Zahl ein.");
		System.out.println("Wenn sie langsamer fahren wollen, geben sie bitte eine negative Zahl ein.");
		System.out.println("Sie können jederzeit das spiel mit der Zahl 0 abbrechen");
		System.out.println("Das Spiel startet jetzt.");
	}
	
	public static void ausgabeNeueGeschwindigkeit(double v) {
		System.out.println("Ihre neue Geschwindigkeit beträgt: " + v + ". Sie können weitere eingaben Tätigen");
	}
	
	public static void liesUserinput() {
		dv = myScanner.nextDouble();
		if(dv==0) {
			closeGame();
		}
	}
	
	public static double beschleunige(double v, double dv) {
		 neueGeschwindigkeit =  v + dv;
		 return (neueGeschwindigkeit < 0) ? 0 : (neueGeschwindigkeit > 130) ? 130 : neueGeschwindigkeit;
	}
	
	public static void closeGame() {
		System.out.println("Alles klar, das Spiel endet jetzt.");
		System.out.println("Hab einen schönen Tag!");
		weiterSpielen = false;
		System.exit(0);
	}
}
