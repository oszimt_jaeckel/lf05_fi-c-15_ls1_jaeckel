import java.util.Scanner;

public class Urlaub {
	static Scanner myScanner;
	static boolean ausfuehren = true, subtractMoney = true;
	static double money, moneyToSpend;
	
	public static void main(String[] args) {
		myScanner = new Scanner(System.in);
		
		money = readDouble("Wieviel Geld haben sie zur Verfügung? ");
		
		while(ausfuehren) {
			// check if money is more than 100, else exit program
			checkMoney(money);
			
			// 
			System.out.print("Bitte geben sie ein Land (USA, Japan, England, Schweiz, Schweden) an: ");
			String inputCountry = myScanner.next();
			
			// get money that will get spend
			moneyToSpend = readDouble("Wieviel Geld soll umgerechnet werden? ");
			
			while(moneyToSpend > money) {
				System.out.println("Sie haben leider nicht mehr so viel Geld :(");
				moneyToSpend = readDouble("Wieviel Geld soll umgerechnet werden? ");
			}
			
			// print money in given currency
			System.out.println(giveCurrency(inputCountry, moneyToSpend));
			
			// if land is valid subtract spend money
			if(subtractMoney) {
				money -= moneyToSpend;
			}
		}
		
	}
	
	// print text + read double from User input
	public static double readDouble(String text) {
		System.out.print(text);
		return myScanner.nextDouble();
	}
	
	// check if user has enough money in reserve
	public static void checkMoney(double money) {
		if(money <= 100) {
			ausfuehren = false;
			System.out.println("Tut mir leid, sie können kein Geld mehr ausgeben.");
			System.out.println("Ich hoffe sie hatten einen schönen Urlaub.");
			System.exit(0);
		}
	}
	
	//check which currency is used and output the money in that currency
	public static String giveCurrency(String inputCountry, double money) {
		String currency = "";
		double currencyMoney = money;
		switch (inputCountry.toUpperCase()) {
			case "USA":
				currencyMoney *= 1.22;
				currency = " USD (Dollar)";
				break;
			case "JAPAN":
				currencyMoney *= 126.50;
				currency = " JPY (Yen)";
				break;
			case "ENGLAND":
				currencyMoney *= 126.50;
				currency = " GBP (Pfund)";
				break;
			case "SCHWEIZ":
				currencyMoney *= 126.50;
				currency = " CHF (Schweizer Franken)";
				break;
			case "SCHWEDEN":
				currencyMoney *= 126.50;
				currency = " SEK (Schwedische Kronen)";
				break;
			default:
				subtractMoney = false;
				return "Tut mir leid, dieses Land kenne ich nicht";
		}
		subtractMoney = true;
		return money + " Euro sind " + currencyMoney + currency;
	}
}
