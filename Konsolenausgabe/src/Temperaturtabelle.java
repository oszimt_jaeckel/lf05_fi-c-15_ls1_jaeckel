
public class Temperaturtabelle {
	
	public static void main(String[] args) {
		System.out.printf("%-12s", "Fahrenheit");
        System.out.print("|");
        System.out.printf("%11s%n", "Celsius ");

        System.out.printf("%24s%n", "------------------------");
        
        System.out.printf("%-12s", "-20");
        System.out.print("|");
        System.out.printf("%4s", "");
        System.out.printf("%.2f %n", -23.8889);
        
        System.out.printf("%-12s", "-10");
        System.out.print("|");
        System.out.printf("%4s", "");
        System.out.printf("%.2f %n", -23.3333);
        
        System.out.printf("%-12s", "+0");
        System.out.print("|"); 
        System.out.printf("%4s", "");
        System.out.printf("%.2f %n", -17.7778);
        
        System.out.printf("%-12s", "+20");
        System.out.print("|");
        System.out.printf("%5s", "");
        System.out.printf("%.2f %n", -6.6667);
        
        System.out.printf("%-12s", "+30");
        System.out.print("|");
        System.out.printf("%5s", "");
        System.out.printf("%.2f %n", -1.1111);

	}
}
