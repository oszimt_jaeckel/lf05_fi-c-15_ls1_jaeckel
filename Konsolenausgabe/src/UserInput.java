import java.util.Scanner;

public class UserInput {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);  // init Userinput Scanner
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		
		int zahl1 = myScanner.nextInt(); // get from the Userinput the next Int
		
		System.out.print("Bitte geben Sie eine zweite, ganze Zahl ein: ");
		
		int zahl2 = myScanner.nextInt();  // get from the Userinput the next Int
		
		int ergebnisAddition = zahl1 + zahl2;
		int ergebnisSubtraktion = zahl1 - zahl2;
		int ergebnisMultiplikation = zahl1 * zahl2;
		int ergebnisDivision = zahl1 / zahl2;
		
		System.out.print("\n\nDas Ergebniss der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAddition);
		
		System.out.print("\nDas Ergebniss der Addition lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSubtraktion);
		
		System.out.print("\nDas Ergebniss der Addition lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMultiplikation);
		
		System.out.print("\nDas Ergebniss der Addition lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDivision);
		
		myScanner.close(); // Close scanner 
	}
}
