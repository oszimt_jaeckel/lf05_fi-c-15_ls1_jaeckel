import java.util.Scanner;

public class UserInfos {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);  // init Userinput Scanner
		System.out.println("Hallo Unbekannt, wir benötigen Folgende Daten von ihnen:");
		System.out.print("\nBitte geben Sie ihren Namen an: ");
		
		String userName = myScanner.next(); // get from the Userinput the next Int
		
		System.out.print("Bitte geben Sie ihr Alter an: ");
		
		int userAge = myScanner.nextInt();  // get from the Userinput the next Int
		
		System.out.print("\n\nHallo " + userName + ", sie sind " + userAge + " Jahre alt.");
		
		myScanner.close(); // Close scanner
	}
}
