import java.util.Scanner;

public class ZaehlenMitFor {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Dieses Programm gibt die Zahlen von 1 bis n und von n bis 1 aus.");
		System.out.print("Bitte geben sie eine positive Zahl (n) an: ");
		int eingegebeneZahl = userInput.nextInt();
		
		for(int zaehler = 1; zaehler <= eingegebeneZahl; zaehler++) {
			System.out.println("Aktuelle Zahl: " + zaehler);
		}
		
		for(int zaehler = eingegebeneZahl; zaehler >= 1; zaehler--) {
			System.out.println("Aktuelle Zahl: " + zaehler);
		}
		
		userInput.close();
	}
}
