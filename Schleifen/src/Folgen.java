
public class Folgen {

	public static void main(String[] args) {
		int schrittweiteB = 1;
		int schrittweiteD = 4;
		
		System.out.println("Aufgabenstellung: Programmieren Sie Schleifen, welche die folgenden Folgen ausgeben:\n");
		System.out.println("a) 99, 96, 93, ... 12, 9");
		
		for(int i = 99; i >= 3; i -= 3) {
			if(i != 3) {
				System.out.printf("%d, ", i);
			} else {
				System.out.printf("%d%n%n", i);
			}
		}
		
		System.out.println("b) 1, 4, 9, 16, 25, ... 361, 400");
		
		for(int i = 1; i <= 400; i += schrittweiteB) {
			if(i != 400) {
				System.out.printf("%d, ", i);
			} else {
				System.out.printf("%d%n%n", i);
			}
			schrittweiteB += 2;
		}
		
		System.out.println("c) 2, 6, 10, 14, … 98, 102");
		
		for(int i = 2; i <= 102; i += 4) {
			if(i != 102) {
				System.out.printf("%d, ", i);
			} else {
				System.out.printf("%d%n%n", i);
			}
		}
		
		System.out.println("d) 4, 16, 36, 64, 100 … 900, 1024");
		
		for(int i = 4; i <= 1024; i += schrittweiteD) {
			if(i != 1024) {
				System.out.printf("%d, ", i);
			} else {
				System.out.printf("%d%n%n", i);
			}
			schrittweiteD += 8;
		}
		
		System.out.println("e) 2, 4, 8, 16, 32, …, 16384, 32768");
		
		for(int i = 2; i <= 32768; i *= 2) {
			if(i != 32768) {
				System.out.printf("%d, ", i);
			} else {
				System.out.printf("%d%n%n", i);
			}
		}
	}
}
