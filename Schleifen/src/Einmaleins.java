 public class Einmaleins {

    public static void main(String[] args) {
    	for(int y = 0; y <= 10; y++) {
            for(int x = 0; x <= 10; x++) {
                if(y==0) {
                    if(x==0) {
                        System.out.print("      ");
                    } else {
                        System.out.printf(" %3d ", x);
                    }
                } else if(x==0) {
                    System.out.printf(" %2d | ", y);
                } else {
                    System.out.printf(" %3d ", x * y);
                }

                if(x==10) {
                    System.out.print("\n");
                }
            }
        }
    }
}
