import java.util.Scanner;

public class Zaehlen {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		int zaehler = 1;
		
		System.out.println("Dieses Programm gibt die Zahlen von 1 bis n und von n bis 1 aus.");
		
		System.out.print("Bitte geben sie eine positive Zahl (n) an: ");
		int eingegebeneZahl = userInput.nextInt();
		
		while(zaehler <= eingegebeneZahl) {
			System.out.println("Aktuelle Zahl: " + zaehler);
			zaehler++;
		}
		
		zaehler = eingegebeneZahl;
		
		while(zaehler >= 1) {
			System.out.println("Aktuelle Zahl: " + zaehler);
			zaehler--;
		}
		
		userInput.close();
	}
}
