import java.util.Scanner;

public class Treppe {
	
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Dieses Programm gibt ihnen eine Treppe mit ihrer gewählten höhe aus.");
		System.out.print("Treppenhöhe: ");
		
		int treppenhoehe = userInput.nextInt();
		
		System.out.print("Stufenbreite: ");
		
		int stufenbreite = userInput.nextInt();
		
		System.out.println("\n");
		
		for(int y = 0; y <= treppenhoehe; y++) { 
			System.out.printf(String.format("%%%ds %n", stufenbreite * treppenhoehe), "*".repeat(stufenbreite * y));
		}
	}
}
