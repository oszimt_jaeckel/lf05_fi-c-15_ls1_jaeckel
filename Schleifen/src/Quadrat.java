import java.util.Scanner;

public class Quadrat {
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Dieses Programm gibt Ihnen ein Quadrat mit der von Ihnen gewählten Größe aus.");
		System.out.print("Seitenlänge: ");
		
		int seitenlaenge = userInput.nextInt();
		
		for(int i = 0; i < seitenlaenge; i++) {
			if(i == 0 || i == seitenlaenge - 1) {
				System.out.println("*".repeat(seitenlaenge));
			} else {
				System.out.print("*");
				System.out.print(" ".repeat(seitenlaenge - 2));
				System.out.print("*\n");
			}
		}
		
		userInput.close();
	}
}
