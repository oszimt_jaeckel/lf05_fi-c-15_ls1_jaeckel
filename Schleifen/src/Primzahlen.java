public class Primzahlen {
    public static void main(String[] args) {

        boolean isPrimeNumber = true;
        String primeNumbers = "P = {";

        System.out.println("Dieses Programm gibt ihnen alle Primzahlen zwischen 2 und 100 aus.");

        for(int currentNumber = 2; currentNumber <= 100; currentNumber++) {
            for(int divideBy = 2; divideBy < currentNumber - 1; divideBy++) {
                if(currentNumber % divideBy == 0) {
                    isPrimeNumber = false;
                }
            }

            if(isPrimeNumber) {
                primeNumbers += (currentNumber + ", ");
            } else {
                isPrimeNumber = true;
            }
        }

        primeNumbers = primeNumbers.substring(0, primeNumbers.length() - 2);
        primeNumbers += "}";

        System.out.println(primeNumbers);
    }
}