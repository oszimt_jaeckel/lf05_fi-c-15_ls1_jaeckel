import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		int summeA = 0, summeB = 0, summeC = 0;
		
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");
		int begrenzer = userInput.nextInt();
		
		for(int i = 1; i <= begrenzer; i++) {
			summeA += i;
		}
		
		for(int i = 1; i <= begrenzer; i++) {
			summeB += i*2;
		}
		
		for(int i = 1; i <= begrenzer; i++) {
			summeC += i*2-1;
		}
		
		gibErgebnisAus('A', summeA);
		gibErgebnisAus('B', summeB);
		gibErgebnisAus('C', summeC);
	}
	
	public static void gibErgebnisAus(char aufgabenID, int summe) {
		System.out.printf("Die Summe für %s beträgt: %d%n", aufgabenID, summe);
	}
}
