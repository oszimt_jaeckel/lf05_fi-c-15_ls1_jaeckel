import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Geben sie bitte eine Zahl ein: ");
		int zahl = userInput.nextInt();
		
		System.out.printf("Die Quersumme beträgt: %d", quersumme(zahl));
		
		userInput.close();
	}
	
	public static int quersumme(int zahl) {
		if(zahl <= 9) {
			return zahl;
		}
		
		return zahl % 10 + quersumme(zahl/10);
	}
}
