public class Modulo {

	public static void main(String[] args) {
		System.out.println("Folgende Zahlen zwischen 1 und 200 sind nicht durch 7 und 5, aber durch 4 teilbar:");
		
		for(int i = 1; i <= 200; i++) {
			if(i % 7 == 0) {
				System.out.println(i + " ist durch 7 teilbar.");
			}
			if(i % 4 == 0 && i % 5 != 0) {
				System.out.println(i + " ist nicht durch 5 aber durch 4 teilbar.");
			}
		}
	}
}
