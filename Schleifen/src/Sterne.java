import java.util.Scanner;

public class Sterne {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Bitte geben sie die maximale Anzahl an Sternen an: ");
		int anzahl = userInput.nextInt();
		
		for(int i = 1; i <= anzahl; i++) {
			System.out.println("*".repeat(i));
		}
	}
}
