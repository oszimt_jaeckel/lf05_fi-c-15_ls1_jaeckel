import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Wie viele Zahlen wollen sie eingeben?");
		int anzahl = userInput.nextInt();
		int zaehler = 0;
		double summe = 0;
		
		while(zaehler < anzahl) {
			System.out.print("Geben Sie eine Zahl ein: ");
			summe += userInput.nextDouble();
			zaehler++;
		}
		
		double mittelwert = summe / anzahl;
		
		System.out.println("Der Mittelwert lautet: " + mittelwert);
	}
}
