import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Matrix");
		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int userNumber = userInput.nextInt();
		int i = 0;
		
		while(i < 100) {
			if(i % 10 == 0) {
				System.out.println();
			}
			if(i % userNumber == 0 && i != 0) {
				System.out.printf("%2s ", "*");
			} else if(numberInNumber(i, userNumber)) {
				System.out.printf("%2s ", "*");
			} else if(quersumme(i) == userNumber) {
				System.out.printf("%2s ", "*");
			} else {
				System.out.printf("%2d ", i);
			}
			
			i++;
		}
		
		userInput.close();
	}
	
	public static boolean numberInNumber(int number, int userNumber) {
		char[] charArray = String.valueOf(number).toCharArray();
		
		for(char currentChar : charArray) {
			if(currentChar == String.valueOf(userNumber).charAt(0)) {
				return true;
			}
		}
		return false;
	}
	
	public static int quersumme(int zahl) {
		if(zahl <= 9) {
			return zahl;
		}
		
		return zahl % 10 + quersumme(zahl/10);
	}
}
