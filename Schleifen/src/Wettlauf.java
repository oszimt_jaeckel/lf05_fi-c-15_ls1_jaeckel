
public class Wettlauf {

	public static void main(String[] args) {
		int sekunde = 0;
		double playerA = 0;
		int playerB = 250;
		
		System.out.printf("%-10s %-10s %-10s%n", "Sekunde", "Spieler A", "Spieler B");
		
		
		while(playerA < 1000 && playerB < 1000) {
			System.out.printf("%-10s %-10.1f %-10d%n", sekunde, playerA, playerB);
			
			sekunde++;
			playerA += 9.5;
			playerB += 7;
		}
		
		System.out.printf("%-10s %-10.1f %-10d%n%n%n", sekunde, playerA, playerB);
		
		if(playerA >= 1000 && playerB < 1000) {
			System.out.println("Spieler A gewinnt!");
		} else if (playerB >= 1000 && playerA < 1000)  {
			
		} else {
			System.out.println("Unentschieden!");
		}
	}
}
