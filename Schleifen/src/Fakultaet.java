import java.util.Scanner;

public class Fakultaet {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		int falkutaet = 1;
		int zaehler = 1;
		
		System.out.println("Dieses Programm berechnet die Fakultät von n.");
		System.out.print("Bitte geben sie eine Zahl (n) zwischen 0 und 20 an: ");
		int zuBerechnendeFalkutaet = userInput.nextInt();
		
		if(zuBerechnendeFalkutaet < 0 || zuBerechnendeFalkutaet > 20) {
			System.out.println("Error: Es wurde eine ungültige Zahl eingegeben.");
			System.out.println("Es wird die Falkutät von 1 ausgegeben");
			zuBerechnendeFalkutaet = 1;
		}
		
		if(zuBerechnendeFalkutaet != 0) {
			do {
				falkutaet *= zaehler;
				zaehler++;
			} while(zaehler <= zuBerechnendeFalkutaet);
		}
		
		System.out.println("Die Falkutät von " + zuBerechnendeFalkutaet + " ist " + falkutaet);
		
		userInput.close();
	}
}
