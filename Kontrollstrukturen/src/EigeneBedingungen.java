import java.util.Scanner;

public class EigeneBedingungen {
	
	private static Scanner userInput = new Scanner(System.in);
	
	public static void main(String[] args) {
		zweiZahlen();
		dreiZahlen();
	}
	
	public static void zweiZahlen() {
		int zahl1 = liesInt("Bitte geben sie eine ganze Zahl an.");
		int zahl2 = liesInt("Bitte geben sie eine weitere, ganze Zahl an.");
		
		gleich(zahl1, zahl2);
		kleiner(zahl1, zahl2);
		groesserGleich(zahl1, zahl2);
	}
	
	public static void dreiZahlen() {
		int zahl1 = liesInt("Bitte geben sie eine ganze Zahl an.");
		int zahl2 = liesInt("Bitte geben sie eine weitere, ganze Zahl an.");
		int zahl3 = liesInt("Bitte geben sie eine weitere, ganze Zahl an.");
		
		groesserUndGroesser(zahl1, zahl2, zahl3);
		groesserOderGroesser(zahl1, zahl2, zahl3);
		groessteZahl(zahl1, zahl2, zahl3);
	}
	
	public static int liesInt(String text) {
		System.out.println(text);
		return userInput.nextInt();
	}
	
	public static void gleich(int zahl1, int zahl2) {
		if(zahl1==zahl2) {
			System.out.println("Die Zahlen sind gleich");
		}
	}
	
	public static void kleiner(int zahl1, int zahl2) {
		if(zahl1<zahl2) {
			System.out.println("Die zweite Zahl ist größer als die erste Zahl.\n");
		}
	}
	
	public static void groesserGleich(int zahl1, int zahl2) {
		if(zahl1>=zahl2) {
			System.out.println("Die erste Zahl ist größer oder gleich der zweite Zahl.\n");
		} else {
			System.out.println("Die erste Zahl ist kleiner als die zweite Zahl.\n");
		}
	}
	
	public static void groesserUndGroesser(int zahl1, int zahl2, int zahl3) {
		if(zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Die erste Zahl ist die größte Zahl.\n");
		}
	}
	
	public static void groesserOderGroesser(int zahl1, int zahl2, int zahl3) {
		if(zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println(zahl3 + " ist Größer als " + zahl2 + " oder " + zahl1 + ".\n");
		}
	}
	
	public static void groessteZahl(int zahl1, int zahl2, int zahl3) {
		if(zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println(zahl1 + " ist die Größte Zahl\n");
		} else if(zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.println(zahl2 + " ist die Größte Zahl\n");
		} else if(zahl3 > zahl1 && zahl3 > zahl2) {
			System.out.println(zahl3 + " ist die Größte Zahl\n");
		} else {
			System.out.println("Mindestens 2 zahlen sind gleich :/");
		}
	}
}
