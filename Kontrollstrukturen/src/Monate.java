import java.util.Scanner;

public class Monate {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		int monat = liesInt("Bitte geben sie den Monat als Zahl an: ", userInput);
		String monatName = checkMonat(monat);
		
		printNotenInfo(monat, monatName);
		
		userInput.close();
	}
	
	public static int liesInt(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextInt();
	}
	
	public static String checkMonat(int monat) {
		switch (monat) {
			case 1:
				return "Januar";
			case 2:
				return "Februar";
			case 3:
				return "M�rz";
			case 4:
				return "April";
			case 5:
				return "Mai";
			case 6:
				return "Juni";
			case 7:
				return "Juli";
			case 8:
				return "August";
			case 9:
				return "September";
			case 10:
				return "Oktober";
			case 11:
				return "November";
			case 12:
				return "Dezember";
			default:
				return "Fehler";
		}
	}
	
	public static void printNotenInfo(int monat, String monatName) {
		if(!monatName.equals("Fehler")) {
			System.out.println("Der " + monat + ". Monat hei�t " + monatName + ".");
		} else {
			System.out.println("Ung�liger Monat: " + monat);
		}
		
	}
}
