import java.util.Scanner;

public class OhmschesGesetz {

	public static void main(String[] args) {
		double R, U, I, ergebnis;
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Hallo, ich bin ein rechner für das Ohmsche Gesetz. Gewünschte Werte können R, I oder U sein.");
		
		char gewuenschteGroesse = liesChar("Bitte geben sie den gewünschten Wert an: ", userInput);
		
		switch (Character.toUpperCase(gewuenschteGroesse)) {
			case 'R':
				U = liesDouble("Bitte geben sie einen Wert für U an: ", userInput);
				I = liesDouble("Bitte geben sie einen Wert für I an: ", userInput);
				ergebnis = rechneGroesse(U, '/', I);
				break;
			case 'U':
				R = liesDouble("Bitte geben sie einen Wert für R an: ", userInput);
				I = liesDouble("Bitte geben sie einen Wert für I an: ", userInput);
				ergebnis = rechneGroesse(R, '*', I);
				break;
			case 'I':
				U = liesDouble("Bitte geben sie einen Wert für U an: ", userInput);
				R = liesDouble("Bitte geben sie einen Wert für R an: ", userInput);
				ergebnis = rechneGroesse(U, '/', R);
				break;
			default:
				ergebnis = -1;
		}
		
		if(ergebnis==-1) {
			System.out.println("Ungültige eingabe");
		} else {
			System.out.println(gewuenschteGroesse + " = " + ergebnis);
		}
		
		System.out.println("\nBis zum nächsten mal :)");
		
		userInput.close();
	}
	
	public static char liesChar(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.next().charAt(0);
	}
	
	public static double liesDouble(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextDouble();
	}
	
	public static double rechneGroesse(double wert1, char operator, double wert2) {
		if(operator=='*') {
			return wert1 * wert2;
		} else {
			return wert1 / wert2;
		}
	}
}
