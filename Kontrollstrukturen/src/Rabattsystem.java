import java.util.Scanner;

public class Rabattsystem {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		double nettoPreis = liesDouble("Bitte geben sie den Nettopreis an: ", userInput);
		double bruttoPreis = getBruttoPreis(nettoPreis);
		
		System.out.println("\nIhre Rechnung beträgt " + bruttoPreis + "€ brutto.");
		userInput.close();
	}
	
	public static double liesDouble(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextDouble();
	}
	
	public static double getBruttoPreis(double nettoPreis) {
		if(nettoPreis>=0 && nettoPreis<= 100) {
			return nettoPreis * 0.9;
		} else if (nettoPreis>100 && nettoPreis<=500) {
			return nettoPreis * 0.85;
		} else {
			return nettoPreis * 0.8;
		}
		
	}
}
