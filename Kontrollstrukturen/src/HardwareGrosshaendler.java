import java.util.Scanner;

public class HardwareGrosshaendler {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		int anzahlMaeuse = liesInt("Wieviele Mäuse wollen sie bestellen?", userInput);
		double nettoPreis = liesDouble("\nWie teuer ist eine Maus?", userInput);
		
		double bruttoPreis = getBruttoPreis(anzahlMaeuse, nettoPreis);
		
		System.out.println(anzahlMaeuse + " Mäuse kosten " + bruttoPreis + "€ brutto.");
		
		userInput.close();
	}
	
	public static int liesInt(String text, Scanner userInput) {
		System.out.println(text);
		return userInput.nextInt();
	}
	
	public static double liesDouble(String text, Scanner userInput) {
		System.out.println(text);
		return userInput.nextDouble();
	}
	
	public static double getBruttoPreis(int anzahlMaeuse,double nettoPreis) {
		double bruttoPreis = (anzahlMaeuse * nettoPreis) * 1.19;
		if(anzahlMaeuse<10) {
			bruttoPreis += 10;
		}
		return bruttoPreis;
	}
}
