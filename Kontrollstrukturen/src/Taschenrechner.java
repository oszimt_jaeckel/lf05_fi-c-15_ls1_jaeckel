import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		double zahl1 = liesDouble("Bitte geben sie eine Zahl an: ", userInput);
		double zahl2 = liesDouble("Bitte geben sie eine weitere Zahl an: ", userInput);
		char operator = liesChar("Bitte geben sie den Operator an: ", userInput);

		double ergebnis = rechne(zahl1, zahl2, operator);
		
		printErgebnis(zahl1, zahl2, operator, ergebnis);
		
		userInput.close();
	}
	
	public static double liesDouble(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextDouble();
	}
	
	public static char liesChar(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.next().charAt(0);
	}
	
	public static double rechne(double zahl1, double zahl2, char operator) {
		switch (operator) {
			case '+':
				return zahl1 + zahl2;
			case '-':
				return zahl1 - zahl2;
			case '*':
				return zahl1 * zahl2;
			case '/':
				return zahl1 / zahl2;
			default:
				return -1;
		}
	}
	
	public static void printErgebnis(double zahl1, double zahl2, char operator, double ergebnis) {
		if(ergebnis!= -1) {
			System.out.println("Das Ergebnis von " + zahl1 + " " + operator + " " + zahl2 + " = " + ergebnis + ".");
		} else {
			System.out.println("Ungültiger Operator: " + operator);
		}
	}
}
