import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		printInfoText();
		
		int jahr = liesInt("Bitte geben sie das gewŁnschte Jahr ein: ", userInput);
		boolean schaltjahr = checkJahr(jahr);
		
		printEndText(jahr, schaltjahr);
	}
	
	public static void printInfoText() {
		System.out.println("Dieses Programm gibt ihnen an, ob ihr eingegebenes Jahr ein Schaltjahr ist, oder nicht.");
	}
	
	public static int liesInt(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextInt();
	}
	
	public static boolean checkJahr(int jahr) {
		if(jahr%4==0) {
			if(jahr%100!=0) {
				return true;
			} else if(jahr%400==0) {
				return true;
			}
		}
		return false;
	}
	
	public static void printEndText(int jahr, boolean schaltjahr) {
		String schaltjahrInfo = (schaltjahr) ? "ist ein Schaltjahr" : "ist kein Schaltjahr";
		System.out.println(jahr + " " + schaltjahrInfo + ".");
	}
}
