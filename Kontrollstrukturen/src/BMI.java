import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		boolean validGeschlecht = false;
		char geschlecht = 'x';
		
		double koerperGewicht = liesDouble("Bitte geben sie ihr Gewicht in kg an: ", userInput);
		double koerperGroesse = liesDouble("\nBitte geben sie ihre Körpergröße in cm an: ", userInput);
		
		while(!validGeschlecht) {
			geschlecht = liesChar("\nBitte geben sie ihr Geschlecht (m/w) an: ", userInput);
			validGeschlecht = checkGeschlecht(geschlecht);
		}
		
		double bmi = getBMI(koerperGewicht, koerperGroesse);
		String klassifikation = getKlassifikation(bmi, geschlecht);
		
		System.out.printf("Ihr BMI ist %.2f, damit sind sie im " + klassifikation + ".", bmi);
	}
	
	public static double liesDouble(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextDouble();
	}
	
	public static char liesChar(String text, Scanner userInput) {
		System.out.println(text);
		return userInput.next().charAt(0);
	}
	
	public static boolean checkGeschlecht(char geschlecht) {
		return (geschlecht == 'm' || geschlecht == 'w') ? true : false;
	}
	
	public static double getBMI(double koerperGewicht, double koerperGroesse) {
		koerperGroesse = koerperGroesse * 0.01;
		return koerperGewicht / (koerperGroesse * koerperGroesse); 
	}
	
	public static String getKlassifikation(double bmi, char geschlecht) {
		if(geschlecht == 'm' ) {
			return (bmi<20) ? "Untergewicht" : (bmi<25) ? "Normalgewicht" : "Übergewicht";
		} else {
			return (bmi<19) ? "Untergewicht" : (bmi<24) ? "Normalgewicht" : "Übergewicht";
		}
	}
}
