import java.util.Scanner;


public class RoemischeZahlenZwei {
	
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		String roemischeZahlenkette = liesString("Bitte geben sie ihre Römische Zahlenkette an: ", userInput);
		roemischeZahlenkette = roemischeZahlenkette.toUpperCase();

		boolean valid = validateInput(roemischeZahlenkette);
		if(valid) {
			
			int[] zahlenArray = getIntZahlenkette(roemischeZahlenkette);
			int ergebnis = rechneRoemischeZahlen(zahlenArray);
			
			System.out.println(roemischeZahlenkette + " ist gleich " + ergebnis);
		} else {
			System.out.println(roemischeZahlenkette + " ist inkorrekt formatiert.");
		}
		
		userInput.close();
	}
	
	public static String liesString(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextLine();
	}
	
	public static boolean validateInput(String roemischeZahlenkette) {
		boolean valid = false;
		int romischeZahlenketteLaenge = roemischeZahlenkette.length();
		for(int i = 0; i < roemischeZahlenkette.length(); i++) {
			boolean oneMoreElement = (i+1 == roemischeZahlenkette.length());
			boolean threeMoreElements = (i+3 == romischeZahlenketteLaenge || i+2 == romischeZahlenketteLaenge);
			switch (roemischeZahlenkette.charAt(i)) {
			case 'I':
				valid = quadrupleCheck(roemischeZahlenkette, i, oneMoreElement, threeMoreElements);
				if(valid && !oneMoreElement) {
					switch(roemischeZahlenkette.charAt(i+1)) {
						case 'I':
							break;
						case 'V':
							break;
						case 'X':
							break;
						default:
							valid = false;
					}
					
				}
				break;
			case 'V':
				valid = !(!oneMoreElement && roemischeZahlenkette.charAt(i+1) == 'V');
				break;
			case 'X':
				valid = quadrupleCheck(roemischeZahlenkette, i, oneMoreElement, threeMoreElements);
				if(valid && !oneMoreElement) {
					switch(roemischeZahlenkette.charAt(i+1)) {
						case 'D':
							valid = false;
							break;
						case 'M':
							valid = false;
							break;
					}
					
				}
				break;
			case 'L':
				valid = !(!oneMoreElement && roemischeZahlenkette.charAt(i+1) == 'L');
				break;
			case 'C':
				valid = quadrupleCheck(roemischeZahlenkette, i, oneMoreElement, threeMoreElements);
				break;
			case 'D':
				valid = !(!oneMoreElement && roemischeZahlenkette.charAt(i+1) == 'D');
				break;
			case 'M':
				valid = quadrupleCheck(roemischeZahlenkette, i, oneMoreElement, threeMoreElements);
				break;
			default:
				valid = false;
			}
			if(!valid) {
				return valid;
			}
		}
		return valid;
	}
	
	public static int[] getIntZahlenkette(String roemischeZahlenkette) {
		int[] zahlenArray = new int[roemischeZahlenkette.length()];
		for(int i = 0; i < roemischeZahlenkette.length(); i++) {
			switch (roemischeZahlenkette.charAt(i)) {
				case 'I':
					zahlenArray[i] = 1;
					break;
				case 'V':
					zahlenArray[i] = 5;
					break;
				case 'X':
					zahlenArray[i] = 10;
					break;
				case 'L':
					zahlenArray[i] = 50;
					break;
				case 'C':
					zahlenArray[i] = 100;
					break;
				case 'D':
					zahlenArray[i] = 500;
					break;
				case 'M':
					zahlenArray[i] = 1000;
					break;
			}
		}
		return zahlenArray;
	}
	
	public static int rechneRoemischeZahlen(int[] zahlenArray) {
		int ergebnis = zahlenArray[0];
		int j = 0, cache = 0;
		for(int i = 0; i < zahlenArray.length - 1; i++) {
			if(zahlenArray[i] >= zahlenArray[i+1]) {
				ergebnis += zahlenArray[i+1];
			} else {
				j = i;
				while(j > -1 && zahlenArray[j] < zahlenArray[i+1]) {
					cache += zahlenArray[j];
					j--;
				}
				ergebnis += zahlenArray[i+1] - cache - zahlenArray[i];
				cache = 0;
			}
		}
		return ergebnis;
	}
	
	public static boolean quadrupleCheck(String roemischeZahlenkette, int currentIndex, boolean oneMoreElement, boolean threeMoreElements) {
		boolean valid = false;
		if(!threeMoreElements && !oneMoreElement) {
			for(int j = currentIndex; j < currentIndex + 3; j++) {
				if(roemischeZahlenkette.charAt(j) == roemischeZahlenkette.charAt(j+1) ) {
					valid = true;
				} else {
					valid = true;
					break;
				}
				if(valid && currentIndex + 3 == j + 1) {
					valid = false;
				} else {
					valid = true;
				}
			}
		} else {
			valid = true;
		}
		return valid;
	}
}
