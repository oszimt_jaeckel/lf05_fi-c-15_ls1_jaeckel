import java.util.Scanner;

public class Funktionsloeser {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		double x = liesDouble("Bitte geben sie einen Wert für x an: ", userInput);
		String bereich = getBereich(x);
		
		double y = getY(x, bereich);
		
		System.out.println("\nDer bereich des wertes ist " + bereich + ", y = " + y);
		
		userInput.close();
	}
	
	public static double liesDouble(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextDouble();
	}
	
	public static String getBereich(double x) {
		if (x <= 0) {
			return "exponentiell";
		} else if (x > 0 && x <= 3) {
			return "quadratisch";
		} else {
			return "linear";
		}
	}
	
	public static double getY(double x, String bereich) {
		switch (bereich) {
		case "exponentiell":
			return Math.pow(2.718, x);
		case "quadratisch":
			return (x * x) + 1;
		case "linear":
			return 2 * x + 4;
		default:
			return 0;
		}
	}
}
