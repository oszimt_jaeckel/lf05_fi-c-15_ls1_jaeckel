import java.util.Scanner;

public class Sortieren {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		char wertEins = liesChar("Bitte geben sie einen Buchstaben / eine Ziffer an: ", userInput);
		char wertZwei = liesChar("Bitte geben sie einen weiteren Buchstaben / eine weitere Ziffer an: ", userInput);
		char wertDrei = liesChar("Bitte geben sie einen weiteren Buchstaben / eine weitere Ziffer an: ", userInput);
		
		char[] werteArray = {wertEins, wertZwei, wertDrei};
		
		werteArray = sortArray(werteArray);
		
		System.out.println(werteArray[0] + " " + werteArray[1] + " " + werteArray[2]);
	}
	
	public static char liesChar(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.next().charAt(0);
	}
	
	public static char[] sortArray(char[] werteArray) {
		char saveChar;
		for(int i = 0; i < werteArray.length; i++) {
			for(int j = 0; j < werteArray.length - 1; j++) {
				if(werteArray[j] > werteArray[j+1]) {
					saveChar = werteArray[j];
					werteArray[j] = werteArray[j+1];
					werteArray[j+1] = saveChar;
				}
			}
		}
		return werteArray;
	}
}
