import java.util.Scanner;

public class Rom {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		char zeichen = liesChar("Bitte geben sie ein r�misches Zahlzeichen ein: ", userInput);
		int zeichenAlsZahl = checkZeichen(zeichen);
		
		printErgebnis(zeichen, zeichenAlsZahl);
		
		userInput.close();
	}
	
	public static char liesChar(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.next().charAt(0);
	}
	
	public static int checkZeichen(char zeichen) {
		switch (zeichen) {
			case 'I':
				return 1;
			case 'V':
				return 5;
			case 'X':
				return 10;
			case 'L':
				return 50;
			case 'C':
				return 100;
			case 'D':
				return 500;
			case 'M':
				return 1000;
			default:
				return -1;
		}
	}
	
	public static void printErgebnis(char zeichen, int zeichenAlsZahl) {
		if(zeichenAlsZahl!=-1) {
			System.out.println(zeichen + " hat den Wert " + zeichenAlsZahl + ".");
		} else {
			System.out.println(zeichen + " ist kein r�misches Zahlzeichen.");
		}
	}
}
