import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		int fahrzeit = 0;
		String ziel = "";
		
		gebAnleitungAus();
		
		char haltInSpandau = liesChar("Hält der Zug in Spandau? ", userInput);
		char richtungHamburg = liesChar("Fährt der Zug richtung Hamburg? ", userInput);
		char haltInStendal = liesChar("Hält der Zug in Stendal? ", userInput);;
		char endetIn = liesChar("Wo endet der Zug? (w = Wolfsburg, b = Braunschweig, h = Hannover) ", userInput);;

		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau

		if (haltInSpandau == 'j') {
			fahrzeit = fahrzeit + 2; // Halt in Spandau
		}

		if (richtungHamburg == 'j') {
			fahrzeit += 96;
			ziel = "Hamburg";
		} else {
			fahrzeit += 34;
			// Fährt der Zug über Stendal
			fahrzeit += (haltInStendal == 'j') ? 16 : 6;
			// Endet der Zug in Hannover oder Braunschweig, wenn nicht in Wolfsburg
			if(endetIn == 'h') {
				fahrzeit += 62;
				ziel = "Hannover";
			} else if (endetIn == 'b') {
				fahrzeit += 50;
				ziel = "Braunschweig";
			} else if (endetIn == 'w') {
				fahrzeit += 29;
				ziel = "Wolfsburg";
			}
		}
		System.out.println("Sie erreichen " + ziel + " nach " + fahrzeit + " Minuten");

	}
	
	public static char liesChar(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.next().charAt(0);
	}
	
	public static void gebAnleitungAus() {
		System.out.println("Willkommen beim Fahrzeit erstellungs tool, geben sie j für Ja und n für Nein ein.\n\n");
	}

}
