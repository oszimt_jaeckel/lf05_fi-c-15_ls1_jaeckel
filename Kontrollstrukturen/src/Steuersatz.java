import java.util.Scanner;

public class Steuersatz {
	
	public static void main(String args[]) {
		boolean validSteuerKlasse = false;
		Scanner userInput = new Scanner(System.in);
		double nettoPreis = 0;
		double bruttoPreis = 0;
		char steuerKlasse;

		
		nettoPreis = liesDouble("Geben sie bitte den Nettopreis des Produktes an.", userInput);
		
		while(!validSteuerKlasse) {
			steuerKlasse = liesChar("Geben sie bitte j für den ermäßtigten Steuersatz an, oder n für den normalen Steuersatz.", userInput);
			if(steuerKlasse == 'j') {
				validSteuerKlasse = true;
				bruttoPreis = nettoPreis * 1.07;
			} else if (steuerKlasse == 'n') {
				validSteuerKlasse = true;
				bruttoPreis = nettoPreis * 1.19;
			}
		}
		
		System.out.println("Der Bruttopreis ihres Produktes beträgt " + bruttoPreis + "€.");
		
		userInput.close();
	}
	
	public static double liesDouble(String text, Scanner userInput) {
		System.out.println(text);
		return userInput.nextDouble();
	}
	
	public static char liesChar(String text, Scanner userInput) {
		System.out.println(text);
		return userInput.next().charAt(0);
	}
}
