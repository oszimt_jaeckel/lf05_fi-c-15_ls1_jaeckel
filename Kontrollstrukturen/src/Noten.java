import java.util.Scanner;

public class Noten {
	
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		int note = liesInt("Bitte geben sie die Note an: ", userInput);
		String notenName = checkNote(note);
		
		printNotenInfo(note, notenName);
		
		userInput.close();
	}
	
	public static int liesInt(String text, Scanner userInput) {
		System.out.print(text);
		return userInput.nextInt();
	}
	
	public static String checkNote(int note) {
		switch (note) {
			case 1:
				return "Sehr gut";
			case 2:
				return "Gut";
			case 3:
				return "Befriedigend";
			case 4:
				return "Ausreichend";
			case 5:
				return "Mangelhaft";
			case 6:
				return "Ungen�gend";
			default:
				return "Keine G�ltige Note";
		}
	}
	
	public static void printNotenInfo(int note, String notenName) {
		System.out.println("\nDie Note " + note + " = " + notenName + ".");
	}
}
