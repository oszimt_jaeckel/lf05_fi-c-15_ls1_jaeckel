import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Formaendern extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Formaendern frame = new Formaendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Formaendern() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{100, 100, 185, 100, 100, 179, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JLabel lblNewLabel = new JLabel("Dieser Text soll ver�ndert werden.");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 1;
		getContentPane().add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{25, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		panel.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{135, 135, 136, 0};
		gbl_panel_1.rowHeights = new int[]{50, 50, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnChangeColorRed = new JButton("Rot");
		btnChangeColorRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		GridBagConstraints gbc_btnChangeColorRed = new GridBagConstraints();
		gbc_btnChangeColorRed.insets = new Insets(0, 0, 5, 5);
		gbc_btnChangeColorRed.gridx = 0;
		gbc_btnChangeColorRed.gridy = 0;
		panel_1.add(btnChangeColorRed, gbc_btnChangeColorRed);
		
		JButton btnChangeColorGreen = new JButton("Gr\u00FCn");
		GridBagConstraints gbc_btnChangeColorGreen = new GridBagConstraints();
		gbc_btnChangeColorGreen.insets = new Insets(0, 0, 5, 5);
		gbc_btnChangeColorGreen.gridx = 1;
		gbc_btnChangeColorGreen.gridy = 0;
		panel_1.add(btnChangeColorGreen, gbc_btnChangeColorGreen);
		
		JButton btnChangeBlue = new JButton("Blau");
		GridBagConstraints gbc_btnChangeBlue = new GridBagConstraints();
		gbc_btnChangeBlue.insets = new Insets(0, 0, 5, 0);
		gbc_btnChangeBlue.gridx = 2;
		gbc_btnChangeBlue.gridy = 0;
		panel_1.add(btnChangeBlue, gbc_btnChangeBlue);
		
		JButton btnChangeColorYellow = new JButton("Gelb");
		GridBagConstraints gbc_btnChangeColorYellow = new GridBagConstraints();
		gbc_btnChangeColorYellow.insets = new Insets(0, 0, 0, 5);
		gbc_btnChangeColorYellow.gridx = 0;
		gbc_btnChangeColorYellow.gridy = 1;
		panel_1.add(btnChangeColorYellow, gbc_btnChangeColorYellow);
		
		JButton btnChangeColorDefault = new JButton("Standardfarbe");
		GridBagConstraints gbc_btnChangeColorDefault = new GridBagConstraints();
		gbc_btnChangeColorDefault.insets = new Insets(0, 0, 0, 5);
		gbc_btnChangeColorDefault.gridx = 1;
		gbc_btnChangeColorDefault.gridy = 1;
		panel_1.add(btnChangeColorDefault, gbc_btnChangeColorDefault);
		
		JButton btnChangeColorChooser = new JButton("Farbe w\u00E4hlen");
		GridBagConstraints gbc_btnChangeColorChooser = new GridBagConstraints();
		gbc_btnChangeColorChooser.gridx = 2;
		gbc_btnChangeColorChooser.gridy = 1;
		panel_1.add(btnChangeColorChooser, gbc_btnChangeColorChooser);
	}

}
