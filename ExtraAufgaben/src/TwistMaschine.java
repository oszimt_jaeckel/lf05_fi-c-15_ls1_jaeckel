import java.io.File;
import java.awt.Font;
import java.awt.Color;
import java.awt.GraphicsEnvironment;
import javax.swing.UIManager;


public class TwistMaschine {
	
	public static void main(String[] args) throws Exception {
		// register custom Fonts
		addFonts();
		
		// set defaults for javax.swing components
		setDefaultFont();
		setDefaultColors();
		
		new GuiTwist();
	}
	
	private static void addFonts() throws Exception {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        
        // register Custom Fonts 
        ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("resources/fonts/Lato-Regular.ttf")));
        ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("resources/fonts/Lato-Light.ttf")));
        ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("resources/fonts/Lato-Bold.ttf")));
    }
	
	private static void setDefaultFont() {
		Font latoFont = new Font("Lato", Font.PLAIN, 14);
		Font latoBoldFont = new Font("Lato", Font.BOLD, 14);
		
		UIManager.put("Label.font", latoFont);
		UIManager.put("TextArea.font", latoFont);
		UIManager.put("Button.font", latoFont);
		UIManager.put("RadioButton.font", latoFont);		
		UIManager.put("OptionPane.font", latoBoldFont);
		UIManager.put("OptionPane.buttonFont", latoBoldFont);
		UIManager.put("OptionPane.messageFont", latoFont);
	}
	
	private static void setDefaultColors() {
		Color textColor = new Color(19, 20, 38);
		Color highlightColor = new Color(96, 55, 245);
		

		UIManager.put("Label.foreground", textColor);
		UIManager.put("TextArea.foreground", textColor);
		UIManager.put("OptionPane.foreground", textColor);
		UIManager.put("RadioButton.foreground", textColor);
		UIManager.put("TextArea.foreground", textColor);
		UIManager.put("Button.background", highlightColor);
		UIManager.put("Button.foreground", Color.WHITE);
	}
}
