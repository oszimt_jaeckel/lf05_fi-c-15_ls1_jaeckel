import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JRadioButton;
import javax.swing.JFileChooser;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;


public class GuiTwist extends JFrame {
	
	//TODO: Radio buttons zu klasse, allgemein alles mehr trennen (vllt)
	
	private String buttonState;
	private Twist twister;
	
	// declare GUI components
	private JPanel mainPanel;
	private JPanel optionButtonPanel;
	private ButtonGroup auswahlButtonGroup;
	private JRadioButton twistButton;
	private JRadioButton enttwistButton;
	private JButton submitButton;
	private JButton exchangeButton;
	
	// new stuff
	private GuiTextArea inputTextArea, outputTextArea;
	

	public GuiTwist() {
		setTitle("Twister");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setSize(new Dimension(540, 450));
		setResizable(false);
		
		initComponents();
		configButtons();
		configComponents();
		
		mainPanel.setLayout(new GridBagLayout());
		
		optionButtonPanel.add(twistButton);
		optionButtonPanel.add(enttwistButton);
		
		mainPanel.add(optionButtonPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		mainPanel.add(inputTextArea, new GridBagConstraints(0, 1, 1, 2, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 25, 0, 0), 0, 0));
		mainPanel.add(exchangeButton, new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		mainPanel.add(outputTextArea, new GridBagConstraints(0, 4, 1, 2, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 25, 0, 0), 0, 0));
		mainPanel.add(submitButton, new GridBagConstraints(0, 6, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		add(mainPanel);
		
		setVisible(true); 
	}
	
	private void initComponents() {
		buttonState = "";
		twister = new Twist();

		// init GUI Components
		mainPanel = new JPanel();
		optionButtonPanel = new JPanel();
		auswahlButtonGroup = new ButtonGroup();
		twistButton = new JRadioButton("Twisten");
		enttwistButton = new JRadioButton("Enttwisten");
		submitButton = new JButton("Umwandel");
		exchangeButton = new JButton("Felder tauschen");
		inputTextArea = new GuiTextArea("Eingabe: ", "resources/images/file-arrow-up-solid.png", "resources/images/eraser-solid.png");
		outputTextArea = new GuiTextArea("Ausgabe: ", "resources/images/file-arrow-down-solid.png", "resources/images/eraser-solid.png");
	}
	
	private void configComponents() {
		outputTextArea.setTextfieldEditable(false);
	}
	
	private void configButtons() {
		// add action listeners to buttons 
		twistButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonState = "Twisten";
			}
		});

		enttwistButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonState ="Enttwisten"; 
			}
		});
		
		submitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(buttonState == "Twisten") {
					twistInput();
				} else if(buttonState == "Enttwisten") {
					enttwistInput();
				}  else {
					JOptionPane.showMessageDialog(mainPanel, "Bitte wähle eine Option aus.");
				}
			}
		});
		
		exchangeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String storage = inputTextArea.getTextfieldText();
				inputTextArea.setTextfieldText(outputTextArea.getTextfieldText());
				outputTextArea.setTextfieldText(storage);
			}
		});
		
		inputTextArea.addTopBtnActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				JFileChooser fileChooser = new JFileChooser();
				
				// add custom filter to JFileChooser (only make .txt files choosable)
				fileChooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
				fileChooser.setAcceptAllFileFilterUsed(false);
				
				int chosenOption = fileChooser.showOpenDialog(null);
				
				// if file was chosen, set input textfield to the content of file
				if(chosenOption == JFileChooser.APPROVE_OPTION) {
					try {
						String fileContent = new String(Files.readString(Paths.get(fileChooser.getSelectedFile().getPath())));
						inputTextArea.setTextfieldText(fileContent);
					} catch (IOException exception) {
						JOptionPane.showMessageDialog(mainPanel, exception, "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		inputTextArea.addBottomBtnActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// clear text field
				inputTextArea.setTextfieldText("");
			}
		});
		
		outputTextArea.addTopBtnActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				JFileChooser fileChooser = new JFileChooser();
				int chosenOption = fileChooser.showSaveDialog(null);
				
				// if file location was chosen, save content of the outputTextArea textfield to a file
				if(chosenOption == JFileChooser.APPROVE_OPTION) {
					try {
						FileWriter fileWriter = new FileWriter(fileChooser.getSelectedFile());
						
						fileWriter.write(outputTextArea.getTextfieldText());
						fileWriter.close();
					} catch (IOException exception) {
						JOptionPane.showMessageDialog(mainPanel, exception, "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		outputTextArea.addBottomBtnActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// clear text field
				outputTextArea.setTextfieldText("");
			}
		});
		
		// set tool tips for all buttons
		twistButton.setToolTipText("Twistet das Wort, dass Sie im Eingabebereich eingegeben haben.");
		enttwistButton.setToolTipText("Entwistet das Wort, dass Sie im Eingabebereich eingegeben haben. "
									+ "Der erste & letzte Buchstabe des getwisteten Wortes muss richtig sein.");
		submitButton.setToolTipText("Twistet / Enttwistet das Wort aus dem Eingabebereich");
		exchangeButton.setToolTipText("Tauscht den Text vom Eingabe- & Ausgabebereich");

		// add custom border to buttons
		submitButton.setBorder(BorderFactory.createEmptyBorder(7, 12, 7, 12));
		exchangeButton.setBorder(BorderFactory.createEmptyBorder(7, 12, 7, 12));
		
		// make custom colors visible
		submitButton.setOpaque(true);
		exchangeButton.setOpaque(true);
		
		
		// group buttons together
		auswahlButtonGroup.add(twistButton);
		auswahlButtonGroup.add(enttwistButton);
	}

	private void twistInput() {
		try {
			outputTextArea.setTextfieldText(twister.twistWords(inputTextArea.getTextfieldText()));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(mainPanel, e, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void enttwistInput() {
		try {
			outputTextArea.setTextfieldText(twister.enttwistWords(inputTextArea.getTextfieldText()));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(mainPanel, e, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
