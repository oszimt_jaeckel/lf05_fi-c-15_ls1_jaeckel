import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.BorderFactory;
import java.awt.event.ActionListener;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;


public class GuiTextArea extends JPanel {
	
	private JPanel mainPanel;
	private JLabel descriptionLabel;
	private JTextArea textfield;
	private JButton topBtn, bottomBtn;
	private ImageIcon topBtnIcon, bottomBtnIcon;
	
	
	public GuiTextArea() {
		this.mainPanel = new JPanel();
		this.descriptionLabel = new JLabel();
		this.textfield = new JTextArea();
		this.topBtn = new JButton();
		this.bottomBtn = new JButton();
		
		configComponents();
		addComponents();	
	}
	
	public GuiTextArea(String description, String topBtnIconPath, String bottomBtnIconPath) {
		this.mainPanel = new JPanel();
		this.descriptionLabel = new JLabel(description);
		this.textfield = new JTextArea(7, 30);
		this.topBtnIcon = new ImageIcon(topBtnIconPath);
		this.bottomBtnIcon = new ImageIcon(bottomBtnIconPath);
		this.topBtn = new JButton(new ImageIcon(topBtnIcon.getImage().getScaledInstance(18, 24, Image.SCALE_DEFAULT)));
		this.bottomBtn = new JButton(new ImageIcon(bottomBtnIcon.getImage().getScaledInstance(18, 24, Image.SCALE_DEFAULT)));
		
		configComponents();
		addComponents();
	}
	
	public String getDesriptionLabelText() {
		return descriptionLabel.getText();
	}
	
	public void setDescriptionLabelText(String text) {
		descriptionLabel.setText(text);
	}
	
	public String getTextfieldText() {
		return textfield.getText();
	}
	
	public void setTextfieldText(String text) {
		textfield.setText(text);
	}
	
	public void setTextfieldEditable(boolean isEditable) {
		textfield.setEditable(isEditable);
	}
	
	public void addTopBtnActionListener(ActionListener function) {
		topBtn.addActionListener(function);
	}
	
	public void addBottomBtnActionListener(ActionListener function) {
		bottomBtn.addActionListener(function);
	}
	
	private void configComponents() {
		mainPanel.setLayout(new GridBagLayout());
		
		// add custom borders
		topBtn.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));
		bottomBtn.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));
		textfield.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		// make custom colors visible
		descriptionLabel.setOpaque(true);
		textfield.setOpaque(true);
		topBtn.setOpaque(true);
		bottomBtn.setOpaque(true);
		
		// make automatic linebreaks in textfield (JTextArea)
		textfield.setLineWrap(true);
		textfield.setWrapStyleWord(true);
	}
	
	private void addComponents() {
		mainPanel.add(descriptionLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(5, 0, 0, 10), 0, 0));
		mainPanel.add(textfield, new GridBagConstraints(1, 0, 1, 5, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		mainPanel.add(topBtn, new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		mainPanel.add(bottomBtn, new GridBagConstraints(2, 3, 1, 1, 1.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		add(mainPanel);
	}
}
