import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.IOException;



public class Twist {
	
	private final String[] woerterArray;
	
	public Twist() {
		this.woerterArray = readFile("resources/woerterliste.txt");
	}
	
	public Twist(String fileLocation) {
		this.woerterArray = readFile(fileLocation);
	}
	
	private String[] readFile(String fileLocation) {
		// temp ArrayList, saves every row of the given file as a string
		ArrayList<String> wordList = new ArrayList<String>();
		try {
			FileInputStream fileInput = new FileInputStream(fileLocation);
			Scanner fileScanner = new Scanner(fileInput);
			// add each word from the file to the wordList
			while(fileScanner.hasNextLine()) {
				wordList.add(fileScanner.nextLine());
			}
			// close filescanner
			fileScanner.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		// return woerterliste as String[]
		return wordList.toArray(new String[0]);
	}
	
	private boolean validWord(String word) {
		// return false if word contains any number or a punctuation that isn't at the end of the word, else true
		if(word.matches("\\w*\\d{1,}\\w*")) return false;
		if(word.matches(".*\\W{1,}.*")) {
			int wordLength = word.length();
			if(wordLength > 1 && word.substring(0, wordLength - 1).matches(".*\\W{1,}.*")) return false;
			
			return true;
		}
		
		return true;
	}
	
	public String twistWords(String text) throws Exception {
		String twistedText = "";
		String[] textArray = text.split(" ");
		
		for(int i = 0; i < textArray.length; i++) {
			int currentWordLength = textArray[i].length();
			
			// if word is longer than 3 chars and is valid, twist it
			// else just append it to the twistedText
			if(validWord(textArray[i]) && currentWordLength > 3) {
				// if the last char of our current word is a punctuation, save it and append the twisted word with the punctuation to twistedText
				// else twist word and append it to the twistedText 
				if(textArray[i].substring(textArray[i].length() - 1).matches("\\W")) {
					char savedChar = textArray[i].charAt(textArray[i].length() -1);
					
					twistedText += twistWord(textArray[i].substring(0, textArray[i].length() - 1));
					twistedText += savedChar + " ";
				} else {
					twistedText += twistWord(textArray[i]) + " ";
				}
			} else {
				twistedText += textArray[i] + " ";
			}
		}
		
		
		return twistedText;
	}
	
	public String twistWord(String word) throws Exception {
		// throw error if text contains multiple words
		if(word.contains(" ")) {
			throw new Exception("Bitte geben Sie nur ein Wort ein!");
		}

		String[] textArray = word.split("");
		String cache = "";
		int durchlauf = 0; // zählt die durchläufe vom while
		
		// if lengt of word is 4, do only possible change and return it
		if(textArray.length == 4) {
			cache = textArray[1];
			textArray[1] = textArray[2];
			textArray[2] = cache;
			
			return String.join("", textArray);
		}

		// solange text gleich textArray ist, twist text
		while (durchlauf < 10) {
			for(int i = textArray.length - 2; i > 2; i--) {
				// generiere eine Zufallszahl zwischen 1 <= z <= i
				int zufallsZahl = (int) ((Math.random() * (i - 1)) + 1);
	
				// tausche textArray[i] mit textArray[zufallsZahl]
				cache = textArray[zufallsZahl];
				textArray[zufallsZahl] = textArray[i];
				textArray[i] = cache;
			}
			// return twistet wort wenn arrays unterschiedlich sind
			if(!Arrays.equals(textArray, word.split(""))) {
				// convert String[] zu String und gib String zurück
				return String.join("", textArray);
			} else {
				durchlauf++;
			}
		}
		return word; 	// falls es nicht twistbar war, gib Original text zurück
	}
	
	public String enttwistWords(String text) throws Exception {
		String enttwistedText = "";
		String[] textArray = text.split(" ");
		
		for(int i = 0; i < textArray.length; i++) {
			String enttwistedWord = "";
			int currentWordLength = textArray[i].length();
			
			// if word is longer than 3 chars and is valid, twist it
			// else just append it to the twistedText
			if(validWord(textArray[i]) && currentWordLength > 3) {
				// if the last char of our current word is a punctuation, save it and append the twisted word with the punctuation to twistedText
				// else twist word and append it to the twistedText 
				if(textArray[i].substring(textArray[i].length() - 1).matches("\\W")) {
					char savedChar = textArray[i].charAt(textArray[i].length() -1);
					enttwistedWord = enttwistWord(textArray[i].substring(0, textArray[i].length() - 1));
					enttwistedWord += savedChar + " ";
				} else {
					enttwistedWord += enttwistWord(textArray[i]) + " ";
				}

				// wenn erster Buchstabe des Wortes groß geschrieben wurde enttwistedWord auch am Anfang groß schreiben, sonst klein
				if(Character.isUpperCase(textArray[i].charAt(0))) {
					enttwistedWord = Character.toUpperCase(enttwistedWord.charAt(0)) + enttwistedWord.substring(1);
				} else {
					enttwistedWord = Character.toLowerCase(enttwistedWord.charAt(0)) + enttwistedWord.substring(1);
				}
				
				// gerade getwistedes wort an getwisteden text anhängen
				enttwistedText += enttwistedWord;
			} else {
				enttwistedText += textArray[i] + " ";
			}
		}
		
		
		return enttwistedText;
	}
	
	public String enttwistWord(String twistedWort) throws Exception {
		// gibt fehler aus wenn mehrere Wörter eingegeben wurden
		if(twistedWort.contains(" ")) {
			throw new Exception("Bitte geben Sie nur ein Wort ein!");
		}
		
		
		// für jedes wort im woerterArray, vergleiche Wort
		for(String wort : woerterArray) {
			// speichere twistedWort als CharArray
			char[] twistedWortCharArray = twistedWort.toUpperCase().toCharArray();
			// speichere aktuelles wort als CharArray
			char[] wortCharArray = wort.toUpperCase().toCharArray();
			boolean wortGleichTwistedWort = true;
			
			
			// wenn Char Arrays gleich lang, vergleiche weiter
			if(wortCharArray.length == twistedWortCharArray.length) {
				// wenn erster und letzer Char aus Arrays gleich, vergleiche weiter
				if(wortCharArray[0] == twistedWortCharArray[0] && wortCharArray[wortCharArray.length -1] == twistedWortCharArray[twistedWortCharArray.length -1]) {
					// sortiere beide Char Arrays
					Arrays.sort(wortCharArray);
					Arrays.sort(twistedWortCharArray);
					/* 
					 * für jeden Char im Array, vergleiche Char von wortCharArray 
					 * mit Char aus twistedWortCharArray 
					 */
					for(int i = 0; i < wortCharArray.length; i++) {
						// wenn ungleich, beende for schleife
						if(wortCharArray[i] != twistedWortCharArray[i]) {
							wortGleichTwistedWort = false;
							break;
						}
					}
					// wenn nach for schleife gleich, return das wort
					if(wortGleichTwistedWort) {
						return wort;
					}
				}
			}
		}
		// gebe fehler aus, wenn wort nicht enttwisten werden konnte
		throw new Exception(twistedWort + " konnte nicht Enttwistet werden!"); 
	}
}
