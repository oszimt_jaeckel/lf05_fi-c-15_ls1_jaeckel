import java.util.Scanner;

public class Palindrom {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		char[] userChars = new char[5];
		
		System.out.println("Dieses Programm liest erst 5 Zeichen ein, und gibt diese dann in umgekehrter Reigenfolge aus");
		
		for(int i = 0; i < 5; i++) {
			System.out.printf("Bitte geben sie das %d. Zeichen ein: ", i + 1);
			userChars[i] = userInput.next().charAt(0);
		}
		
		for(int i = 4; i >= 0; i--) {
			System.out.println(userChars[i]);
		}
		
		userInput.close();
	}
}
