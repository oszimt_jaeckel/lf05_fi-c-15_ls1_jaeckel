
public class Lotto {

	public static void main(String[] args) {
		int[] lottoZahlen = { 3, 7, 12, 18, 37, 42 };
		
		System.out.print("[ ");
		
		for(int i = 0; i < lottoZahlen.length; i++) {
			System.out.print(i + " ");
		}
		
		System.out.println("]");
		
		checkIfInArray(lottoZahlen, 12);
		checkIfInArray(lottoZahlen, 13);
	}
	
	public static void checkIfInArray(int[] zahlenArray, int gesuchteZahl) {
		boolean inArray = false;
		
		for(int i = 0; i < zahlenArray.length; i++) {
			if(zahlenArray[i] == gesuchteZahl) {
				inArray = true;
			}
		}
		
		if(inArray) {
			System.out.printf("Die Zahl %d ist in der Ziehung enthalten.%n", gesuchteZahl);
		} else {
			System.out.printf("Die Zahl %d ist nicht in der Ziehung enthalten.%n", gesuchteZahl);
		}
	}
}
