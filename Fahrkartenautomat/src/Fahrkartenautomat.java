﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
      
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
 
       // Geldeinwurf
       // -----------
       double eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
           eingezahlterGesamtbetrag += fahrkartenBezahlen(zuZahlenderBetrag);
       }

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}

    
    public static double fahrkartenbestellungErfassen() {
    	// init Input Scanner
    	Scanner tastatur = new Scanner(System.in);
    	// get cost of ticket
    	System.out.print("Zu zahlender Betrag (EURO): ");
    	double ticketPreis = tastatur.nextDouble();
    	
    	if (ticketPreis <= 0) {
    		ticketPreis = 1;
    		System.out.println("Error: Ungültiger Ticketpreis. Es wird mit 1 Euro gerechnet.");
    	}
    	
    	// get numbers of tickets
    	System.out.print("Anzahl der Tickets: ");
        short anzahlTickets = tastatur.nextShort();

        if (anzahlTickets < 1 || anzahlTickets > 10) {
        	anzahlTickets = 1;
        	System.out.println("Error: Ungültige Ticketanzahl. Es wird ein Ticket gekauft.");
        }
    	// return costs of tickets
    	return ticketPreis * anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
		// init Input Scanner
		Scanner tastatur = new Scanner(System.in);
		// get money input
		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
		double eingeworfeneMünze = tastatur.nextDouble();
		// return input money
		return eingeworfeneMünze;
    }
    
    public static void fahrkartenAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag;
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
           
        }
        System.out.println("\n\n");
        
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
        if(rückgabebetrag > 0.0) {
     	   rueckgeldAusgeben(rückgabebetrag);
        }
        
        
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
		"Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro %n", rückgabebetrag);
 	   	System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
        	muenzeAusgeben(2, "EURO");
	        rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
        	muenzeAusgeben(1, "EURO");
	        rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
        	muenzeAusgeben(50, "CENT");
	        rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
        	muenzeAusgeben(20, "CENT");
	        rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
    	  	muenzeAusgeben(10, "CENT");
            rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
        	muenzeAusgeben(5, "CENT");
	        rückgabebetrag -= 0.05;
        }
    }
    
    public static void warte(int milisekunden) {
    	try {
 			Thread.sleep(milisekunden);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
}

/*
 * A2.5 Aufgabe 1, 2, 5, 6
 *
 *	Zu 1.
 *		Objekt (Scanner): tastatur
 *		Double: zuZahlenderBetrag
 *		Double: eingezahlterGesamtbetrag
 *		Double: eingeworfeneMünze
 *		Double: rückgabebetrag
 *	Zu 2.
 *		= (einfache Zuweisung)
 *		+ (Summe)
 *		- (Differenz)
 *		< (Kleiner als)
 *		> (Größer als)
 *		>= (Größer gleich)
 *		+= (Additionszuweisung)
 *		-= (Subtraktionszuweisung)
 *	Zu 5. 
 *		Ich habe einen short genommen, da die Ticketanzahl niemals eine Dezimalzahl sein sollte 
 *		und eine Ticket Ausgabe von über 32.767 nicht vorkommen würde im normalen Betrieb.
 *	Zu 6.
 *		Bei der Berechnung Anzahl * Einzelpreis speichere ich den Wert der Multiplikation von den
 *		beiden Variablen in der Variable zuZahlenderBetrag, um anschließend den Preis aller Tickets
 *		nutzen zu können.
*/